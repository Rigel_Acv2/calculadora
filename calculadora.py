def menu():
    print("Opciones: \n"
          "1 - Sumar \n"
          "2 - Restar \n"
          "3 - Dividir \n"
          "4 - Multiplicar \n")

def operaciones():

    menu()
    opcion =int(input("Diga la opcion que quiere utilizar: "))

    if (opcion ==1):
        valor1 = int(input("introduzca primer valor: "))
        valor2 = int(input("introduzca segundo valor: "))

        total = valor1 + valor2
        print(valor1, " + ", valor2, " = ", total)

    elif opcion == 2:
        valor1 = int(input("introduzca primer valor: "))
        valor2 = int(input("introduzca segundo valor: "))

        total = valor1 - valor2
        print(valor1, " - ", valor2, " = ", total)

    elif opcion == 3:
        valor1 = int(input("introduzca primer valor: "))
        valor2 = int(input("introduzca segundo valor: "))

        total = valor1 / valor2
        print(valor1, " / ", valor2, " = ", total)

    elif opcion == 4:
        valor1 = int(input("introduzca primer valor: "))
        valor2 = int(input("introduzca segundo valor: "))

        total = valor1 * valor2
        print(valor1, " * ", valor2, " = ", total)

    else:
        print("\n No es una de las opciones. Intente nuevamente \n")
        operaciones()

operaciones()